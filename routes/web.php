<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/mizzycook', function () {
    return view('mizzycook');
});

Route::get('/foodies', function () {
    return view('foodies');
});

Route::get('/foodieReport', function () {
    return view('foodieReport');
});

Route::get('/next', function () {
    return view('next');
});

Route::get('/meet', function () {
    return view('meet');
});
