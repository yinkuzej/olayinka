@extends('layouts.app')

@section('content')

    <section class="row content-wrap single-nosidebar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 single-post-contents">
                    <article class="single-post-content v2 v2p1 row m0 post">
                        <header class="row">
                            <div class="row">
                                <h5 class="taxonomy pull-left"><i>in</i> <a href="#">image</a>, <a href="#">entertainment</a></h5>
                                <div class="response-count pull-right"><img src="images/comment-icon-gray.png" alt="">5</div>
                            </div>
                            <h2 class="post-title">Nature, in the broadest sense, is the natural, physical, or material world or universe.</h2>
                            <h5 class="post-meta">
                                By <a href="#">Mark Sanders</a> | <a href="#" class="date">feb 17, 2016</a>
                            </h5>
                        </header>
                        <div class="featured-content row m0">
                            <a href="#"><img src="images/featured-posts/3.jpg" alt=""></a>
                        </div>
                        <div class="post-content row">
                            <div class="row m0">
                                <div class="col-sm-4 post-author-about">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#"><img src="images/author.png" alt="" class="img-circle"></a>
                                        </div>
                                        <div class="media-body">
                                            <h3><a href="#">Mark Sanders</a></h3>
                                            <h5>small title</h5>
                                            <ul class="nav social-nav white">
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>The word nature is derived from the Latin word natura, or "essential qualities, innate disposition", and in ancient times, literally meant "birth".</p>
                                </div>
                                <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>
                            </div>
                            <br>
                            <h3>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h3>
                            <br>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <br>
                            <blockquote class="has-sign row m0">
                                <div>
                                    <p>If everybody learns this simple art of loving his work, whatever it is, enjoying it without asking for any recognition, we would have more beautiful and celebrating world.</p>
                                    <footer>OSHO</footer>
                                </div>
                            </blockquote>
                            <h3>Sunt in culpa qui officia deserunt mollit anim id est laborum.</h3>
                            <br>
                            <ul class="triangle-list nav">
                                <li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>
                                <li>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>
                            </ul>
                            <ul class="circle-list nav">
                                <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</li>
                                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                            </ul>
                            <br>
                            <blockquote class="no-sign">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            </blockquote>
                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                                <div class="col-sm-4">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                </div>
                                <div class="col-sm-4">
                                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                            <br>
                            <!-- 16:9 aspect ratio -->
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Gi3ZT4JttVI"></iframe>
                            </div>
                            <br>

                            <!-- Place somewhere in the <body> of your page -->
                            <div class="thumbCarousel row m0">
                                <div id="slider" class="flexslider">
                                    <ul class="slides">
                                        <li><img src="images/posts/gallery/l1.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l2.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l3.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l1.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l2.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l3.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/l1.jpg" alt=""></li>
                                    </ul>
                                </div>
                                <div id="carousel" class="flexslider">
                                    <ul class="slides">
                                        <li><img src="images/posts/gallery/s1.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s2.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s3.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s1.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s2.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s3.jpg" alt=""></li>
                                        <li><img src="images/posts/gallery/s1.jpg" alt=""></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div class="row m0 tags">
                            <a href="#" class="tag">music</a>
                            <a href="#" class="tag">tegs</a>
                            <a href="#" class="tag">tegs hover</a>
                        </div>

                        <ul class="pager">
                            <li>
                                <h4>Previous Articles</h4>
                                <h2 class="post-title"><a href="single2.html">Nature, in the broadest sense, is the natural, physical...</a></h2>
                                <h5 class="taxonomy pull-left"><i>in</i> <a href="#">image</a>, <a href="#">entertainment</a></h5>
                            </li>
                            <li>
                                <h4>Next Articles</h4>
                                <h2 class="post-title"><a href="single2.html">Nature, in the broadest sense, is the natural, physical...</a></h2>
                                <h5 class="taxonomy pull-left"><i>in</i> <a href="#">image</a>, <a href="#">entertainment</a></h5>
                            </li>
                        </ul>

                        <div class="row m0 comments">
                            <h5 class="response-count">5 comments<a href="#comment-form" class="btn btn-primary pull-right"><span>add comment</span></a></h5>
                            <!--Comments-->
                            <div class="media comment">
                                <div class="media-left">
                                    <a href="#"><img src="images/posts/c1.jpg" alt="" class="img-circle"></a>
                                </div>
                                <div class="media-body">
                                    <h4><a href="#">Mark Sanders</a></h4>
                                    <h5><a href="#" class="date">feb 17, 2016 at 3:30pm</a> | <a href="#" class="reply-link">reply</a></h5>
                                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                    <!--Comments-->
                                    <div class="media comment reply">
                                        <div class="media-left">
                                            <a href="#"><img src="images/posts/c2.jpg" alt="" class="img-circle"></a>
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="#">Mark Sanders</a></h4>
                                            <h5><a href="#" class="date">feb 17, 2016 at 3:30pm</a> | <a href="#" class="reply-link">reply</a></h5>
                                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                            <!--Comments-->
                                            <div class="media comment reply">
                                                <div class="media-left">
                                                    <a href="#"><img src="images/posts/c1.jpg" alt="" class="img-circle"></a>
                                                </div>
                                                <div class="media-body">
                                                    <h4><a href="#">Mark Sanders</a></h4>
                                                    <h5><a href="#" class="date">feb 17, 2016 at 3:30pm</a> | <a href="#" class="reply-link">reply</a></h5>
                                                    <p>Remaining essentially unchanged.</p>
                                                    <!--Comments-->
                                                    <div class="media comment reply">
                                                        <div class="media-left">
                                                            <a href="#"><img src="images/posts/c2.jpg" alt="" class="img-circle"></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4><a href="#">Mark Sanders</a></h4>
                                                            <h5><a href="#" class="date">feb 17, 2016 at 3:30pm</a> | <a href="#" class="reply-link">reply</a></h5>
                                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Comments-->
                                <div class="media comment">
                                    <div class="media-left">
                                        <a href="#"><img src="images/posts/c1.jpg" alt="" class="img-circle"></a>
                                    </div>
                                    <div class="media-body">
                                        <h4><a href="#">Mark Sanders</a></h4>
                                        <h5><a href="#" class="date">feb 17, 2016 at 3:30pm</a> | <a href="#" class="reply-link">reply</a></h5>
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="#" method="post" class="comment-form row" id="comment-form">
                            <h5 class="form-title">leave a reply</h5>
                            <div class="form-group col-sm-6">
                                <label for="name">full name*</label>
                                <input type="text" id="name" class="form-control" placeholder="Your name" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">full name*</label>
                                <input type="email" id="email" class="form-control" placeholder="Your email address here" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="website">website</label>
                                <input type="url" id="website" class="form-control" placeholder="Your website url" >
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="subject">subject</label>
                                <input type="text" id="subject" class="form-control" placeholder="Write subject here" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="message">message</label>
                                <textarea name="message" id="message" class="form-control" placeholder="Write message here"></textarea>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="btn-primary"><span>send</span></button>
                                <h5 class="pull-right">*required fields</h5>
                            </div>
                        </form>
                    </article>
                </div>
            </div>
        </div>
    </section>

@endsection